var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);


server.listen(3000, function() {
    console.log('Listening on :3000');
});

var players  = [];
var playersHash = {};
var potatoes = [];
var gameState = 'lobby';
var timeToPlay = 10;
var minPlayers = 3;

// var readyPlayers = 0;
// var canJoinAndPlay = true;
// var gameInProgress = false;
var playerWithPotato;
var startTime;
var lastTime;

io.sockets.on('connection', function(socket) {

    console.log('connected with socket id: ' + socket.id);

    //  add the player to the array
    players.push({ id: socket.id, readyState: false, timeThisRound: 0 })

    //  tell everyonne who is connected
    io.emit('players', { players });

    //  handle disconnect
    socket.on('disconnect', function() {
        
        //  remove from players array
        removePlayerById(socket.id);

        socket.broadcast.emit('players', { players })

    });

    socket.on('change-ready-state', function(data) {

        // check if not in play
        if(gameState === 'playing') {
            return;
        }

        //  change state
        changeStateById(socket.id, data);

        //  check if at least half are ready
        var readyCount = 0;
        for (c = 0; c < players.length; c++) {
            if(players[c].readyState) {
                readyCount++;
            }
        }

        if(readyCount / players.length > 0.5 && players.length >= minPlayers) {
            // start game
            io.emit('start', { players })
            startGame();
        }

        io.emit('players', { players });

    });

    function removePlayerById(id) {
        var r = false;
        for (c = 0; c < players.length; c++) {
            if(players[c].id == id) {
                r = c;
            }
        }
        players.splice(r, 1);
    }

    function changeStateById(id, state) {
        var r = false;
        for (c = 0; c < players.length; c++) {
            if(players[c].id == id) {
                r = c;
            }
        }
        players[r].readyState = state;
    }





//     players.push({ id: socket.id, canStart: false, isPlaying: canJoinAndPlay, hasPotato: false, timeThisRound: 0, timeTotal: 0, gameCount: 0 });
//     console.log(players);
//     io.emit('players', players);



//     //  handle is ready to start
//     socket.on('ready', function (data) {
//         console.log('player ' + socket.id + ' ready is ' + data);
//         players[getPlayerIndexFromId(socket.id)].canStart = data;
//         data ? readyPlayers++ : readyPlayers--;
//         io.emit('players', players);
//         console.log('readyPlayers', readyPlayers);

//         //  check if enough players to start
//         if(readyPlayers == minPlayers) {
//             startGame()
//         }
//     })

    //  handle pass event
    socket.on('pass', function() {
        //  check if this player has the potato

        thisTime = Date.now();

        players[getPlayerIndexFromId(socket.id)].hasPotato = false;
        //  log the time
        players[getPlayerIndexFromId(socket.id)].timeThisRound += (thisTime - lastTime);
        //  check if max time exceeded
        if(players[getPlayerIndexFromId(socket.id)].timeThisRound > timeToPlay * 1000) {
            endGame();
        }
        lastTime = thisTime;

        //  choose another random player
        playerWithPotato = pickAPlayer(socket.id);
        players[playerWithPotato].hasPotato = true;
        io.emit('has-potato', { current: players[playerWithPotato].id, players } );
    })


    function getPlayerIndexFromId(id) {
        var r = false;
        for (c = 0; c < players.length; c++) {
            if(players[c].id == id) {
                r = c;
            }
        }
        return r;
    }

    function startGame() {
        
        startTime = lastTime = Date.now();

        //  reset lat round time
        for (c = 0; c < players.length; c++) {
            players[c].timeThisRound = 0;
        }

        // gameInProgress = true;
        // canJoinAndPlay = false;
        // io.emit('countdown', 0);

        playerWithPotato = pickAPlayer();
        console.log('random player', playerWithPotato, players);
        players[playerWithPotato].hasPotato = true;
        // console.log('players in start', players);

        io.emit('has-potato', { current: players[playerWithPotato].id, players } );

        var interval = setInterval(function() {
            var now = Date.now();

            if(now - lastTime > timeToPlay * 1000) {
                //  someone hasn't played
                clearInterval(interval);
                endGame();
            }

            players.forEach(element => {
                // console.log(element.id, element.timeThisRound);
                if(element.timeThisRound > timeToPlay * 1000) {
                    clearInterval(interval);
                    endGame();
                }
            })
            // var elapsedTime = Date.now() - startTime;
        //     // console.log((elapsedTime / 1000).toFixed(3));
        //     if(elapsedTime >= 10000) {
        //         gameInProgress = false;
        //         clearInterval(interval);
        //         endGame()
        //     }
        }, 100);

    }

    function pickAPlayer (previous) {
        var toChooseFrom = [];
        players.forEach(element => {
            if(previous === null || element.id !== previous) {
                toChooseFrom.push(element.id)
            } else {
                element.readyState = false
            }
        });

        var newId = toChooseFrom[Math.floor(Math.random() * toChooseFrom.length)];

        for(var i = 0; i < players.length; i++) {
            if(players[i].id === newId) {
                return i;
            }
        }
    }

    /**
     * endGame
     */
    function endGame() {

        thisTime = Date.now();

        players[playerWithPotato].hasPotato = false;
        //  log the time
        // players[playerWithPotato].timeThisRound += (thisTime - lastTime);

        readyPlayers = 0;

        for (c = 0; c < players.length; c++) {
            players[c].canStart = false;
            players[c].gameCount++;

            players[c].timeTotal += players[c].timeThisRound;
        }

        io.emit('end', players);
    }


});